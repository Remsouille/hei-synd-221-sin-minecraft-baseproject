package ch.hevs.isi.database;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.web.WebConnector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;


/**
 * DatabaseConnector class implements interface DataPointListener
 * This class is following the Singleton pattern meant to update the DataBase values
 */
public class DatabaseConnector implements DataPointListener {
    /**
     * Instanciating a DatabaseConnector object which will be the only object from this class
     */
    private final String sUrl = "https://influx.sdi.hevs.ch:443/write?db=SIn31";
    private final String userpass = "SIn31:47298ef110acefc80b0aa8942dd6b730";

    private static DatabaseConnector database_c = null;
    private DatabaseConnector(){}

    /**
     * Method DataBaseConnector getInstance()
     * It creates the single DatabaseConnector object if it does not exist
     * @return the DatabaseConnector object
     */
    public static DatabaseConnector getInstance() {
        if (database_c == null) { database_c = new DatabaseConnector(); }
        return database_c;
    }

    /**
     * Method pushToDB
     * This method will show the defined message on the console and transmit the label et value to the Influx online database
     * @param label name of the datapoint
     * @param value actual value of the datapoint
     */
    private void pushToDB(String label, String value) {
        System.out.println("pushToDB: " + label + " value=" + value);
        try {
            URL url = new URL(sUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            String encoding = Base64.getEncoder().encodeToString(userpass.getBytes());
            connection.setRequestProperty ("Authorization", "Basic " + encoding);
            connection.setRequestProperty("Content-Type", "binary/octet-stream");
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            String body = "SInWorld "+ label + "=" + value;
            System.out.println("Body: " + body);
            writer.write(body);
            writer.flush();
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            int responseCode = connection.getResponseCode();
            System.out.println(responseCode + " <-- Response code ");
            /*
            while ((in.readLine()) != null) {
                System.out.println(responseCode);
                System.out.println(in);
            }
             */
            connection.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Methode void onNewValue(FloatDataPoint floatDataPoint)
     * This method update DataPoints with float values
     * @param floatDataPoint floatDataPoint who has been updated
     */
    @Override
    public void onNewValue(FloatDataPoint floatDataPoint) {
        pushToDB(floatDataPoint.getLabel(), floatDataPoint.toString());
    }
    /**
     * Methode void onNewValue(BinaryDataPoint booleanDataPoint)
     * This method update DataPoints with boolean values
     * @param booleanDataPoint booleanDataPoint who has been updated
     */
    @Override
    public void onNewValue(BinaryDataPoint booleanDataPoint) {
        pushToDB(booleanDataPoint.getLabel(), booleanDataPoint.toString());
    }

    public static void main(String[] args) throws InterruptedException, IOException {
        WebConnector wb = WebConnector.getInstance();
        wb.start();
        BinaryDataPoint binp1 = new BinaryDataPoint("REMOTE_WIND_SW", true);
        BinaryDataPoint binp2 = new BinaryDataPoint("REMOTE_SOLAR_SW", true);
        FloatDataPoint fdp1 = new FloatDataPoint("SOLAR_P_FLOAT", false);
        FloatDataPoint fdp2 = new FloatDataPoint("COAL_P_FLOAT", false);

        binp1.setValue(false);
        binp2.setValue(false);
        while(true){
            fdp1.setValue(700);
            fdp2.setValue(1700);
            Thread.sleep(5000);
            fdp1.setValue(2000);
            fdp2.setValue(1000);
            Thread.sleep(5000);
            fdp1.setValue(100);
            fdp2.setValue(1800);
            Thread.sleep(5000);
        }
    }
}
