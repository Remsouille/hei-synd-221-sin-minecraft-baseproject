package ch.hevs.isi.field;

import java.nio.ByteBuffer;
/**
 * MbapH class function is to create a fixed size header for communication with SIn world Minecraft
 */
public class MbapH {
    static short transactionID = 0x0001;
    static final short protocolID = 0x0000;
    short frameLength = 0x0000;
    static final byte unitID = 0x01;
    byte[] testArray;
    /**
     * Class constructor that assembles every elements of the header
     * @param theBB is the ByteBuffer used to contains the header and by extension the whole frame
     */
    public MbapH(ByteBuffer theBB){
        frameLength = (short) (theBB.capacity()-6);
        theBB.putShort(0, transactionID);
        theBB.putShort(2, protocolID);
        theBB.putShort(4, frameLength);
        theBB.put(6, unitID);
        transactionID++;
        testArray = theBB.array();
    }
}

