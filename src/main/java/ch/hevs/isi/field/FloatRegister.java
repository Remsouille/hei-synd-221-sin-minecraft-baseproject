package ch.hevs.isi.field;

import ch.hevs.isi.core.FloatDataPoint;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * FloatRegister class inherit from ModbusAccessor class
 * It links the Datapoint objects and the ModBus objects
 * Values received should be only updated and saved inside Datapoints
 */
public class FloatRegister{
        short address;
        FloatDataPoint floatDP = new FloatDataPoint("", false);
        private static Map<String, FloatRegister> floatReg = new HashMap<>();
        String label = "";

    /**
     * @param address is the address contained in the datapoint linked to the register
     * @param floatDP is the datapoint linked with the register
     * @param label corresponds to the labelled ModBus component
     * This constructor instantiates a float register by giving parameters
     */
        public FloatRegister(short address, FloatDataPoint floatDP, String label) {
            this.address = address;
            this.floatDP = floatDP;
            this.label = label;
            floatReg.put(label,this);
        }
    /**
     * poll() method reads iteratively every datapoints from every registers contained in the map floatReg
     * @throws IOException
     */
    public static void poll() throws IOException {
        //Iteration to read every float registers
        for (FloatRegister fr: floatReg.values()){
            fr.read();
            System.out.println(" - Float register address : " + fr.address);
        }
    }

    /**
     * read method is called at a fixed interval (periodic threads)
     * @throws IOException if an error occurs during the read operation
     */
    void read() throws IOException {
            floatDP.setValue(ModbusAccessor.getInstance().setReadHoldingReg(address));
            //System.out.print(floatDP.getValueF());
        }

    /**
     * write method is called whenever a new value has been sent from website/smart control
     * @throws IOException if an error occurs during the write operation
     */
    void write() throws IOException {
            ModbusAccessor.getInstance().setWriteHoldingReg(address, floatDP.getValueF());
        }
    /**
     * This method returns the register from the map corresponding to the datapoint given
     * @param floatDataPoint is the datapoint corresponding to a register
     * @return the register contained in the map floatReg that has the same label as the datapoint
     */
    public static FloatRegister getRegisterFromDataPoint(FloatDataPoint floatDataPoint){
        return floatReg.get(floatDataPoint.getLabel());
    }
}

