package ch.hevs.isi.field;

import ch.hevs.isi.utils.Utility;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * ModbusAccessor class implements the modbus communication between the field component (SIn world) and the client (Website)
 * It allows to read/write specific registers (Boolean values / Float values)
 * This class is following the Singleton pattern
 */
public class ModbusAccessor extends Socket implements Pdu{

    private final static String host_name = "localhost"; //we use localhost as the adress for server
    private final static int port = 1502;   //communicating on port 1502
    static ByteBuffer modbusBuffer1 = ByteBuffer.allocate(12);  //for read/write Coils and read HoldingRegister
    static ByteBuffer modbusBuffer2 = ByteBuffer.allocate(17);  //for write HoldingRegister

    final byte byteCNT = 0x04;      //always reading 2 registers 2*2 = 4
    final short qtyCoils = 0x0001;  //always reading 1 coil at a time
    final short qtyRegs = 0x0002;   //always reading 2 registers at a time

    /**
     * Instanciating a ModbusAccessor object which will be the only object from this class
     */
    private static ModbusAccessor modbus_ac = null;

    /**
     * @param host indicates to which host the communication is aiming
     * @param port gives the port number to communicate with the host
     * this constructor instantiates the communication with a stream (Input and Output stream)
     * It creates also the mbapHeader needed for communicating with the minecraft server
     * @throws IOException if the user enters an invalid parameter it will throws an error
     */
    public ModbusAccessor(String host, int port) throws IOException {   //Constructor for input/output communication
        super(host, port);
        OutputStream _outModBus = this.getOutputStream();
        InputStream _inModBus = this.getInputStream();

        //creates mbap headers
        MbapH mbapHeader1 = new MbapH(modbusBuffer1);
        MbapH mbapHeader2 = new MbapH(modbusBuffer2);

    }

    /**
     * readCoilsByte is called when a ModBus instruction has been sent to read the boolean value at the given address (during the writing sequence) in return
     * @return the value contained in the readBuffer at index 9 which correspond at the boolean value wanted
     * @throws IOException if an error occurs during the reading operation
     */
    public byte readCoilsByte() throws IOException {
        byte[] serverResponse;

        //load the PDU frame into the serverResponse array
        serverResponse = modbusRead(this.getInputStream());
        //if array is not empty then wrap it into readBuffer and get the value
        if (serverResponse != null) {
            ByteBuffer readBuffer = ByteBuffer.wrap(serverResponse);
            if ((readBuffer.get(2)&0x01) == 0) {
                //System.out.println("the coil value is : 0");
            }else{

                //System.out.println("the coil value is : 1");
            }
            return readBuffer.get(2);
        }
        //if array is empty return byte value -1 to signal an error
        return (byte) -1;
    }

    /**
     * readHoldingRegFloat is called when a ModBus instruction has been sent to read the float value at the given address (during the writing sequence) in return
     * @return the variable floatVal1 which contains the float value wanted
     * @throws IOException if an error occurs during the reading operation
     */
    public float readHoldingRegFloat() throws IOException {
        byte[] serverResponse;

        //load the PDU frame into the serverResponse array
        serverResponse = modbusRead(this.getInputStream());

        //if array is not empty then wrap it into readBuffer and get the value
        if (serverResponse != null) {
            ByteBuffer readBuffer = ByteBuffer.wrap(serverResponse);
            //System.out.println(Utility.getHexString(readBuffer.array()) + " PROBLEM");
            float floatVal1 = readBuffer.getFloat(2 );
            //System.out.println("the value is : " + (floatVal1 * 100) + "%");
            return floatVal1;
        }
        //if array is empty return float value -1 to signal an error
        return -1f;
    }

    /**
     * private constructor needed for the singleton pattern
     */
    private ModbusAccessor() {

    } //empty constructor
    /**
     * Method ModbusAccessor getInstance()
     * It creates the single ModbusAccessor object if it does not exist
     * @return the ModbusAccessor object
     */
    public static ModbusAccessor getInstance() throws IOException {
        //if ModBusAccessor object does not exist then instantiates one else return the existing object
        if (modbus_ac == null) { modbus_ac = new ModbusAccessor(host_name, port); }
        return modbus_ac;
    }

    /**
     * when called getRWPDU returns the function code given in the instruction to be send
     * @param myBB is the ByteBuffer containing the ModBus frame and in this case the function code to read
     * @return the function code value contained in the ByteBuffer at the given index
     */
    @Override
    public byte getRWPDU(ByteBuffer myBB) {
        return myBB.get(7);
    }

    /**
     * setWriteCoil method creates the rest of the frame which contains the function code and the different parameters needed
     * when the frame is complete, the frame is sent  over the output stream
     * @param outputAddress is the address corresponding to which component we want to write
     * @param outputValue is the value to write at the outputAddress (Be carefull BIG ENDIAN ORDER ! if you want to write a boolean 1 ==> 0xFF00 and a 0 ==> 0x0000)
     * @throws IOException if the user enters an invalid parameter it will throws an error
     */
    @Override
    public void setWriteCoil(short outputAddress, short outputValue) throws IOException {
        //clear ByteBuffer before sending frame
        modbusBuffer1.clear();

        //creates the PDU frame based on the documentation
        modbusBuffer1.put(7, (byte)0x05);
        modbusBuffer1.putShort(8, outputAddress);
        modbusBuffer1.putShort(10, outputValue);
        //System.out.println(Utility.getHexString(modbusBuffer1.array()) + " <-- full frame array content from modbusBuffer1 writeCoil");

        Utility.sendBytes(this.getOutputStream(), modbusBuffer1.array(), 0, modbusBuffer1.array().length);          //write a bool to a coil

    }

    /**
     * setReadCoil method creates the rest of the frame which contains the function code and the different parameters needed
     * @param startingAddress is the address corresponding to the component from which we want to read the value
     * @return the method readCoilsByte which returns the boolean value contained at the right index
     * @throws IOException if the user enters an invalid parameter it will throws an error
     */
    @Override
    public byte setReadCoil(short startingAddress) throws IOException {
        //clear ByteBuffer before sending frame
        modbusBuffer1.clear();

        //creates the PDU frame based on the documentation
        modbusBuffer1.put(7, (byte) 0x01);
        modbusBuffer1.putShort(8, startingAddress);
        modbusBuffer1.putShort(10, qtyCoils);
        System.out.println(Utility.getHexString(modbusBuffer1.array()) + " <-- full frame array content from modbusBuffer1 readCoil");
        Utility.sendBytes(this.getOutputStream(), modbusBuffer1.array(), 0, modbusBuffer1.array().length);         //read bool value on coil

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
       return readCoilsByte();
    }

    /**
     * setWriteHoldingReg method creates the rest of the frame which contains the function code and the different parameters needed
     * @param outputAddress is the address corresponding to which component we want to write
     * @param regValue is the value to write at the outputAddress
     * @throws IOException if the user enters an invalid parameter it will throws an error
     */
    @Override
    public void setWriteHoldingReg(short outputAddress, float regValue) throws IOException {
        //clear ByteBuffer before sending frame
        modbusBuffer2.clear();

        //creates the PDU frame based on the documentation
        modbusBuffer2.put(7, (byte) 0x10);
        modbusBuffer2.putShort(8, outputAddress);
        modbusBuffer2.putShort(10, qtyRegs);
        modbusBuffer2.put(12, byteCNT);
        modbusBuffer2.putFloat(13, regValue);  // 13 to 16
        System.out.println(Utility.getHexString(modbusBuffer2.array()) + " <-- full frame array content from modbusBuffer1 writeReg");
        Utility.sendBytes(this.getOutputStream(), modbusBuffer2.array(), 0, modbusBuffer2.array().length);       //Write a float value to holding registers

    }

    /**
     * setReadHoldingReg method creates the rest of the frame which contains the function code and the different parameters needed
     * @param startingAddress is the address corresponding to the component from which we want to read the value
     * @return the method readHoldingRegFloat which returns the float value contained at the right index
     * @throws IOException if the user enters an invalid parameter it will throws an error
     */
    @Override
    public float setReadHoldingReg(short startingAddress) throws IOException {
        //clear ByteBuffer before sending frame
        modbusBuffer1.clear();

        //creates the PDU frame based on the documentation
        modbusBuffer1.put(7, (byte)0x03);
        modbusBuffer1.putShort(8, startingAddress);
        modbusBuffer1.putShort(10, qtyRegs);
        System.out.println(Utility.getHexString(modbusBuffer1.array()) + " <-- full frame array content from modbusBuffer1 readReg");

        Utility.sendBytes(this.getOutputStream(), modbusBuffer1.array(), 0, modbusBuffer1.array().length);         //read float value on holding registers


        return readHoldingRegFloat();
    }

    public static void main(String[] args) {
        try {
            //init Socket connection
           ModbusAccessor myAccessor = new ModbusAccessor(host_name, port);
           Thread.sleep(500);

           //gives instruction to test ModBus read/write
           myAccessor.setReadCoil((short) 613);                     //read Wind SW state
           myAccessor.setReadHoldingReg((short) 605);               //read factory ST value
           myAccessor.setWriteCoil((short) 401, (short) 0xFF00);    //write Solar SW state
           myAccessor.setWriteHoldingReg((short) 205,0.2f); //write Factory SP value
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * this method reads the ModBus answer and separates the mbap Header from the PDU
     * @param in corresponds to the Input stream from which we receive the response
     * @return the PDU content, it will be used for updating value
     */
    private synchronized byte[] modbusRead(InputStream in) {
        try {
            // read MBAP header
            while (in.available() < 7) {
                Utility.waitSomeTime(10);
            }
            byte[] mbap = Utility.readNBytes(in, 7);
            ByteBuffer mbapHeader = ByteBuffer.wrap(mbap);
            int pduLength = mbapHeader.getShort(4) - 1;

            if (pduLength > 0) {
                // read PDU
                while (in.available() < pduLength)
                    Utility.waitSomeTime(10);

                return Utility.readNBytes(in, pduLength);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}

