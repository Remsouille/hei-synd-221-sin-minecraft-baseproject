package ch.hevs.isi.field;

import ch.hevs.isi.core.BinaryDataPoint;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * BooleanRegister class inherit from ModbusAccessor class
 * It links the Datapoint objects and the ModBus objects
 * Values received should be only updated and saved inside Datapoints
 */
public class BooleanRegister {
    private short address = 0;
    private BinaryDataPoint boolDP;
    private static Map<String, BooleanRegister> booleanReg = new HashMap<>();
    String label = "";
    /**
     * @param address is the address contained in the datapoint linked to the register
     * @param boolDP is the datapoint linked with the register
     * @param label corresponds to the labelled ModBus component
     * This constructor instantiates a boolean register by giving parameters
     */
    public BooleanRegister(short address, BinaryDataPoint boolDP, String label){
        this.address = address;
        this.boolDP = boolDP;
        this.label = label;
        booleanReg.put(label,this);
    }

    /**
     * poll() method reads iteratively every datapoints from every registers contained in the map booleanReg
     * @throws IOException
     */
    public static void poll() throws IOException {
        //Iteration to read every boolean registers
        for (BooleanRegister br: booleanReg.values()){
            br.read();
            System.out.println(" - Boolean register address : " + br.address);
        }
    }

    /**
     * read method is called at a fixed interval (periodic threads = polling)
     * @throws IOException if an error occurs during the read operation
     */
    void read() throws IOException {
       //send over ModBus the instruction to read value at given address
       boolDP.setValue(ModbusAccessor.getInstance().setReadCoil(address));
       System.out.print(boolDP.getValueB());
    }
    /**
     * write method is called whenever a new value has been sent from website/smart control
     * @throws IOException if an error occurs during the write operation
     */
    void write() throws IOException {
        short bpVal = 0;

        //if value of boolean datapoint is true --> bpVal = 0xFF00
        if (boolDP.getValueB()){
            bpVal = (short) 0xFF00;
        }
        //Write over ModBus the new value
        ModbusAccessor.getInstance().setWriteCoil(address, bpVal);
    }

    /**
     * This method returns the register from the map corresponding to the datapoint given
     * @param booleanDataPoint is the datapoint corresponding to a register
     * @return the register contained in the map booleanReg that has the same label as the datapoint
     */
    public static BooleanRegister getRegisterFromDataPoint(BinaryDataPoint booleanDataPoint){
        return booleanReg.get(booleanDataPoint.getLabel());
    }
}

