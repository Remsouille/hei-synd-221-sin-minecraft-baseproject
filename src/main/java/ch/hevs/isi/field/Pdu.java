package ch.hevs.isi.field;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * This interface is implemented in ModbusAccessor class to set read/write operations frames
 */
public interface Pdu {
    /**
     * return the function code of the given frame contained in the ByteBuffer parameter
     * @param myBB is the ByteBuffer containing the frame from which we want to extract the function code
     * @return the function code of the frame
     */
    byte getRWPDU(ByteBuffer myBB);

    /**
     * setWriteCoil method creates the rest of the frame which contains the function code and the different parameters needed
     * when the frame is complete, the frame is sent  over the output stream
     * @param outputAddress is the address corresponding to which component we want to write
     * @param outputValue is the value to write at the outputAddress (Be carefull BIG ENDIAN ORDER ! if you want to write a boolean 1 = 0xFF00 and a 0 = 0x0000)
     * @throws IOException if the user enters an invalid parameter it will throws an error
     */
    void setWriteCoil(short outputAddress, short outputValue) throws IOException;

    /**
     * setReadCoil method creates the rest of the frame which contains the function code and the different parameters needed
     * @param startingAddress is the address corresponding to the component from which we want to read the value
     * @return the method readCoilsByte which returns the boolean value contained at the right index
     * @throws IOException if the user enters an invalid parameter it will throws an error
     */
    byte setReadCoil(short startingAddress) throws IOException;

    /**
     * setWriteHoldingReg method creates the rest of the frame which contains the function code and the different parameters needed
     * @param startingAddress is the address corresponding to which component we want to write
     * @param regValue is the value to write at the outputAddress
     * @throws IOException if the user enters an invalid parameter it will throws an error
     */
    void setWriteHoldingReg(short startingAddress, float regValue) throws IOException;

    /**
     * setReadHoldingReg method creates the rest of the frame which contains the function code and the different parameters needed
     * @param startingAddress is the address corresponding to the component from which we want to read the value
     * @return the method readHoldingRegFloat which returns the float value contained at the right index
     * @throws IOException if the user enters an invalid parameter it will throws an error
     */
    float setReadHoldingReg(short startingAddress) throws IOException;
}
