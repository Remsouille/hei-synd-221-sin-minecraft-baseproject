package ch.hevs.isi.field;

import ch.hevs.isi.SmartControl.SmartControl;

import java.io.IOException;
import java.util.TimerTask;

/**
 * PollTask class inherit from TimerTask and must implement his "run" method
 * Inside run method we call the poll() method for respectively Boolean and Float registers
 */
public class PollTask extends TimerTask {
    @Override
    /**
     * this method is called periodically to call the polling method
     * for both boolean and float registers
     */
    public void run() {
        try {
            BooleanRegister.poll();
            FloatRegister.poll();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

