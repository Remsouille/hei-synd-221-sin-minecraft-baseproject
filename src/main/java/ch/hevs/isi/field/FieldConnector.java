package ch.hevs.isi.field;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;

import java.io.IOException;
import java.util.Timer;

/**
 * FieldConnector class implements interface DataPointListener
 * This class is following the Singleton pattern meant to update the Field values
 */
public class FieldConnector  implements DataPointListener {
    /**
     * Instanciating a FieldConnector object which will be the only object from this class
     */
    private static FieldConnector field_c = null;
    private FieldConnector(){}
    /**
     * Method FieldConnector getInstance()
     * It creates the single FieldConnector object if it does not exist
     * @return the FieldConnector object
     */
    public static FieldConnector getInstance() {
        if (field_c == null) { field_c = new FieldConnector(); }
        return field_c;
    }

    /**
     * this method creates a Timer which will, every 6000ms, creates a new PollTask
     * to run the polling methods
     */
    public void startPolling(){
        Timer myTimer = new Timer();
        myTimer.scheduleAtFixedRate(new PollTask(), 0, 6000);
    }
    /**
     * Method pushToField
     * This method will show the defined message on the console
     * @param label name of the datapoint
     * @param value actual value of the datapoint
     */
    private void pushToField(String label, String value) {
        System.out.println("pushToField: " + label + "=" + value);
    }

    /**
     * Methode void onNewValue(FloatDataPoint floatDataPoint)
     * This method update DataPoints with float values
     * @param floatDataPoint floatDataPoint who has been updated
     */
    @Override
    public void onNewValue(FloatDataPoint floatDataPoint) throws IOException {
        pushToField(floatDataPoint.getLabel(), floatDataPoint.toString());
        FloatRegister floatR = FloatRegister.getRegisterFromDataPoint(floatDataPoint);
        floatR.write();
    }
    /**
     * Methode void onNewValue(BinaryDataPoint booleanDataPoint)
     * This method update DataPoints with boolean values
     * @param booleanDataPoint booleanDataPoint who has been updated
     */
    @Override
    public void onNewValue(BinaryDataPoint booleanDataPoint) throws IOException {
        pushToField(booleanDataPoint.getLabel(), booleanDataPoint.toString());
        BooleanRegister boolR = BooleanRegister.getRegisterFromDataPoint(booleanDataPoint);
        boolR.write();
    }

}

