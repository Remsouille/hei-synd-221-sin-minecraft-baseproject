package ch.hevs.isi.web;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

/**
 * WebConnector class implements interface DataPointListener
 * This class is following the Singleton pattern meant to update the Web values
 */
public class WebConnector extends WebSocketServer implements DataPointListener {
    /**
     * Instantiating a WebConnector object which will be the only object from this class
     */
    private static WebConnector Web_c = null;
    private static int port = 8888;

    public WebConnector() throws UnknownHostException {
        super(new InetSocketAddress(port));
    }

    /**
     * Methode opOpen(WebSocket webSocket, ClientHandshake clientHandshake)
     * send a answer to the webpage who have joined
     * @param webSocket socket to communication with the webpage
     * @param clientHandshake an unused parameter
     */
    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
    webSocket.send("Welcome to the server");
    System.out.println(webSocket.getRemoteSocketAddress().getAddress().getHostAddress() + " joined the server!");
    }

    /**
     * Methode onClose(WebSocket webSocket, int i, String s, boolean b)
     * is use to close the connection with the webpage
     * @param webSocket socket to communication with the webpage
     * @param i an unused parameter
     * @param s an unused parameter
     * @param b an unused parameter
     */
    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        System.out.println(webSocket.getRemoteSocketAddress().getAddress().getHostAddress() + " has left the server!");
        webSocket.close();
    }

    /**
     * Methode OnMessage(WebSocket webSocket, String s) is used to
     * get data from the webpage
     * @param webSocket socket to communication with the webpage
     * @param s string who contain the message from the webpage
     */
    @Override
    public void onMessage(WebSocket webSocket, String s) {
        System.out.println("New message from : " + webSocket.getRemoteSocketAddress().getAddress().getHostAddress() + " who say : " + s);
    String[] string = s.split("=");
    String label = string[0];
    String value = string[1];
    if(value.contains(".")){
        try {
            FloatDataPoint.getDataPointFromLabel(string[0]).setValue(Float.valueOf(string[1]));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }else{
        try {
            BinaryDataPoint.getDataPointFromLabel(string[0]).setValue(Boolean.parseBoolean(string[1]));
    } catch (IOException e) {
            e.printStackTrace();
        }
    }
    }

    /**
     * Methode OnError(WebSocket webSocket, Exception e) is
     * call when an error happen
     * @param webSocket socket to communication with the webpage
     * @param e the exception
     */
    @Override
    public void onError(WebSocket webSocket, Exception e) {

    }

    /**
     * Methode onStart() is not used
     */
    @Override
    public void onStart() {

    }

    /**
     * Method WebConnector getInstance()
     * It creates the single WebConnector object if it does not exist
     * @return the WebConnector object
     */
    public static WebConnector getInstance() {
        if (Web_c == null) {
            try {
                Web_c = new WebConnector();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
        return Web_c;
    }
    /**
     * Method pushToWebPages
     * This method will show the defined message on the console
     * @param label name of the datapoint
     * @param value actual value of the datapoint
     */
    private void pushToWebPages(String label, String value){
        System.out.println("pushToWeb: " + label + "=" + value);
        this.broadcast(label+"="+value);
    }

    /**
     * Methode void onNewValue(FloatDataPoint floatDataPoint)
     * This method update DataPoints with float values
     * @param floatDataPoint floatDataPoint who has been updated
     */
    @Override
    public void onNewValue(FloatDataPoint floatDataPoint){
        pushToWebPages(floatDataPoint.getLabel(), floatDataPoint.toString());
    }
    /**
     * Methode void onNewValue(BinaryDataPoint booleanDataPoint)
     * This method update DataPoints with boolean values
     * @param booleanDataPoint booleanDataPoint who has been updated
     */
    @Override
    public void onNewValue(BinaryDataPoint booleanDataPoint){
        pushToWebPages(booleanDataPoint.getLabel(), booleanDataPoint.toString());
    }
    public static void main(String[] args) throws InterruptedException, IOException {
        WebConnector wb = WebConnector.getInstance();
        wb.start();
        FloatDataPoint fdp1 = new FloatDataPoint("COAL_AMOUNT", false);
        FloatDataPoint fdp2 = new FloatDataPoint("SOLAR_P_FLOAT", false);
        FloatDataPoint fdp3 = new FloatDataPoint("WIND_P_FLOAT", false);
        FloatDataPoint fdp4 = new FloatDataPoint("COAL_P_FLOAT", false);
        FloatDataPoint fdp5 = new FloatDataPoint("REMOTE_COAL_SP", false);
        FloatDataPoint fdp6 = new FloatDataPoint("REMOTE_FACTORY_SP", false);
        BinaryDataPoint bd1 = new BinaryDataPoint("REMOTE_SOLAR_SW", false);
        BinaryDataPoint bd2 = new BinaryDataPoint("REMOTE_WIND_SW", false);

        while(true){
            fdp1.setValue((float) (100*Math.random()));
            fdp2.setValue((float) (300+700*Math.random()));
            fdp3.setValue((float) (300+700*Math.random()));
            fdp4.setValue((float) (300+700*Math.random()));
            Thread.sleep(5000);
        }
    }

}
