package ch.hevs.isi.launcher;

import ch.hevs.isi.SmartControl.SmartControl;
import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.field.BooleanRegister;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.field.FloatRegister;
import ch.hevs.isi.utils.Utility;
import ch.hevs.isi.web.WebConnector;

import java.io.BufferedReader;

import java.io.IOException;

/**
 * Launcher class function is to instantiate every datapoints and every registers linked to their respective datapoint
 * by reading a .csv file then it will call the FieldConnector method "startPolling()" to update the reading values
 */
public class Launcher{
    public static void main(String[] args) throws IOException {
        WebConnector wb = WebConnector.getInstance();
        wb.start();
        String pathToCsv = "ModbusEA_SIn.csv";
        String dataLine = "";
        BooleanRegister myboolReg = null;
        FloatRegister myfloatReg = null;
        BufferedReader csvReader = Utility.fileParser(null, pathToCsv);
        while (csvReader.ready()) {
            boolean isOutput = false;
            dataLine = csvReader.readLine();
            String[] arrayLine = dataLine.split(";");

            if (arrayLine[4].contains("Y")) {
                isOutput = true;
            }
            if (arrayLine[0].contains("SW")) {
                System.out.println(arrayLine[0] + " " + arrayLine[5]);
                BinaryDataPoint bp = new BinaryDataPoint(arrayLine[0], isOutput);
                myboolReg = new BooleanRegister((short) Integer.parseInt(arrayLine[5]), bp, arrayLine[0]);
            }else{
            System.out.println(arrayLine[0] + " " + arrayLine[5]);
            FloatDataPoint fp = new FloatDataPoint(arrayLine[0], isOutput);
            myfloatReg = new FloatRegister((short) Integer.parseInt(arrayLine[5]), fp, arrayLine[0]);
            }
        }
        FieldConnector.getInstance().startPolling();
        //SmartControl.pollingWeather();
    }
}

