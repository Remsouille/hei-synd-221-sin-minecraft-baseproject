package ch.hevs.isi.SmartControl;

import ch.hevs.isi.field.BooleanRegister;
import ch.hevs.isi.field.FloatRegister;

import java.io.IOException;
import java.util.TimerTask;
/**
 * SmartPoll class inherit from TimerTask and must implement his "run" method
 * Inside run method we call the poll() method for respectively Boolean and Float registers
 */
public class SmartPoll extends TimerTask {

    @Override
    /**
     * this method is called periodically to call the polling method
     * for the SmartControl
     */
    public void run() {
        try {
            SmartControl sc = new SmartControl();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
