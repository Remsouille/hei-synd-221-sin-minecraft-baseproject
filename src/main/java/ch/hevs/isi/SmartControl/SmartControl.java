package ch.hevs.isi.SmartControl;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.field.PollTask;

import java.io.IOException;
import java.util.Timer;

public class SmartControl {
    private final static int weatherNice = 0;
    private final static int weatherRain = 5;
    private final static int weatherRainT = 10;
    private final static int countDownLimit = 5;
    private final static int avgVolt = 800;
    int nomVolt = 0;        //Grid voltage
    float coalST = 0.0f;    //coal setpoint
    boolean solarSW = false;

    float weatherState = 0.0f; //current weather state
    float weatherForecastState = 0.0f; //
    int weatherCountDown = 0;

    /**
     * The constructor of the class is used to do the smart control of the grid
     * @throws IOException
     */
    public SmartControl() throws IOException {
        updateDP();
        int weather = (int) (weatherState*10);


        if (nomVolt < (avgVolt - 50)){
            if(!solarSW){
                FloatDataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW").setValue(true);
            }else {
                if (coalST <= 98) {
                    double test = (coalST + 2) / 100;
                    FloatDataPoint.getDataPointFromLabel("COAL_ST").setValue((coalST + 2) / 100);
                }
            }
        }
        if (nomVolt > (avgVolt + 50)){
            if (coalST >= 2){
                FloatDataPoint.getDataPointFromLabel("COAL_ST").setValue((coalST-2)/100);
            }else{
                FloatDataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW").setValue(false);
            }
        }

        if (weatherCountDown <= countDownLimit) {
            weather = (int) (weatherForecastState * 10);

            switch (weather) {
                case weatherNice:
                    weatherIsNice();
                    break;
                case weatherRain:
                    weatherIsRain();
                    break;
                case weatherRainT:
                    weatherIsRainThunder();
                    break;
                default:
                    System.out.println("An invalid weather value is detected !");
                    break;
            }
        }
    }

    /**
     * When the next weather is nice we can lower the level of the coal power
     * @throws IOException
     */
    public void weatherIsNice() throws IOException {
        if (coalST >= 50 ) {
            FloatDataPoint.getDataPointFromLabel("COAL_ST").setValue((coalST -50) / 100);
        }
    }

    /**
     * This methode is call when the next weather is rain, we can up the level of the coal power
     * @throws IOException
     */
    public void weatherIsRain() throws IOException {
        if (coalST <= 50 ) {
            FloatDataPoint.getDataPointFromLabel("COAL_ST").setValue((coalST +50) / 100);
        }
    }

    /**
     * This methode is call when the next weather is rain and thunder, we can up the level of the coal power
     * @throws IOException
     */
    public void weatherIsRainThunder() throws IOException {
        if (coalST <= 50 ) {
            FloatDataPoint.getDataPointFromLabel("COAL_ST").setValue((coalST +50) / 100);
        }
    }

    /**
     * This methode is used to get the value of some datapoint with which we used to set the level of the coal power
     */
    public void updateDP(){
        nomVolt = (int) (FloatDataPoint.getDataPointFromLabel("GRID_U_FLOAT").getValueF()*1000);
        coalST =  FloatDataPoint.getDataPointFromLabel("COAL_ST").getValueF()*100;
        weatherCountDown = (int) (FloatDataPoint.getDataPointFromLabel("WEATHER_COUNTDOWN_FLOAT").getValueF()*100);
        weatherForecastState =  FloatDataPoint.getDataPointFromLabel("WEATHER_FORECAST_FLOAT").getValueF();
    }

    /**
     * This methode start a polling for the smart control
     */
    public static void pollingWeather(){
        Timer smartTimer = new Timer();
        smartTimer.scheduleAtFixedRate(new SmartPoll(), 0, 5000);
    }
}
