package ch.hevs.isi.core;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.io.IOException;

/**
 * DataPointListener will be implemented in connectors classes to update DataPoints when new values are received
 */
public interface DataPointListener {
    void onNewValue(FloatDataPoint floatDataPoint) throws IOException;
    void onNewValue(BinaryDataPoint booleanDataPoint) throws IOException;
}
