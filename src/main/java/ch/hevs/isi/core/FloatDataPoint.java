package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;
import javafx.scene.chart.PieChart;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * FloatingDataPoint class inherit from DataPoint class
 * This class is used whenever floating values are received/sent
 * and allow to update the differents connectors
 */
public class FloatDataPoint extends DataPoint {

    private float value = 0;
    /**
     * Inherited constructor for dataPoint containing floating values
     * @param label the name given to the datapoint created
     * @param isOutput indcates if datapoint is created for output values or input values
     */
    public FloatDataPoint(String label, boolean isOutput) {
        super(label, isOutput);
    }
    /**
     * Method void setValue(float value)
     * this method update the floating value on the connectors
     * by calling the method onNewValue
     * @param value contains the new float value to be updated
     */
    public void setValue(float value) throws IOException {
        this.value = value;
        DatabaseConnector dc = DatabaseConnector.getInstance();
        dc.onNewValue(this);
        WebConnector wc = WebConnector.getInstance();
        wc.onNewValue(this);
        if (isOutput()) {
            FieldConnector fc = FieldConnector.getInstance();
            fc.onNewValue(this);
        }
    }
    /**
     * Method float getValue()
     * @return the float value contained in variable value
     */
    public float getValueF(){
        return value;
    }


    /**
     * Method String toString()
     * This method convert the value to a string
     * @return the string form of the value
     */
    public String toString(){
        return Float.toString(this.value);
    }

    /**
     * Method float fromString(String requestValue)
     * this method split the string and convert the string value into a float value
     * @param requestValue string to be send
     * @return float value
     */
    public float fromString(String requestValue){
        String[] splitArray = requestValue.split("=");
        return Float.parseFloat(splitArray[1]);
    }



}
