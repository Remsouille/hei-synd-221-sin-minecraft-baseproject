package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

import java.io.IOException;

/**
 * BinaryDataPoint class inherit from DataPoint class
 * This class is used whenever boolean values are received/sent
 * and allow to update the differents connectors
 */
public class BinaryDataPoint extends DataPoint{

    private boolean value;
    /**
     * Inherited constructor for dataPoint containing boolean values
     * @param label the name given to the datapoint created
     * @param isOutput indcates if datapoint is created for output values or input values
     */
    public BinaryDataPoint(String label, boolean isOutput) {
        super(label, isOutput);
    }

    /**
     * Method void setValue(boolean value)
     * this method update the value on the connectors
     * by calling the method onNewValue
     * @param value contains the new boolean value to be updated
     */
    public void setValue(boolean value) throws IOException {
        this.value = value;
        DatabaseConnector dc = DatabaseConnector.getInstance();
        dc.onNewValue(this);
        WebConnector wc = WebConnector.getInstance();
        wc.onNewValue(this);
        if (isOutput()) {
            FieldConnector fc = FieldConnector.getInstance();
            fc.onNewValue(this);
        }
    }

    /**
     * Method boolean getValue()
     * @return the boolean value contained in variable value
     */
    public boolean getValueB(){
        return value;
    }

    /**
     * Method String toString()
     * This method convert the value to a string
     * @return the string form of the value
     */
    public String toString(){
        if(this.value){
            return "1";

        }else{
            return "0";
        }
    }

    /**
     * Method boolean fromString(String requestValue)
     * this method split the string and convert the string value into a boolean value
     * @param requestValue string to be send
     * @return boolean value
     */
    public boolean fromString(String requestValue){
        String[] splitArray = requestValue.split("=");
        return Boolean.parseBoolean(splitArray[1]);
    }
}
