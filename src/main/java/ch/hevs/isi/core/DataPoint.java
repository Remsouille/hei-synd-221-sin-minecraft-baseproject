package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 * Datapoint class allow to create dataPoints
 * by indicating a label name and by giving an orientation about the value
 * it will represent (Is it an output value or an input value ?)
 */
public class DataPoint {
    /**
     * This map will store every dataPoint created based on his label
     */
    private static Map<String, DataPoint> dataPointMap = new HashMap<>();

    private String label= "";
    private boolean isOutput;

    /**
     * Datapoint constructor
     * it is called everytime an update appears on the received/sent values
     * Also the datapoint created is saved in the dataPointMap byy indicating his label
     * @param label the name given to the datapoint created
     * @param isOutput indicates if datapoint is created for output values or input values
     */
    protected DataPoint(String label, boolean isOutput){
        this.label = label;
        this.isOutput = isOutput;
        dataPointMap.put(label,this);
    }

    /**
     * Method DataPoint getDataPointFromLabel(String label)
     * gives back the datapoint based on the parameter
     * @param label the name of the dataPoint wanted
     * @return the correct dataPoint based on the label given
     */
    public static DataPoint getDataPointFromLabel(String label){
        return dataPointMap.get(label);
    }

    /**
     * this method set a new boolean value to the datapoint
     * @param value is the new value for the datapoint
     * @throws IOException
     */
    public void setValue(boolean value) throws IOException {}

    /**
     * this method set a new float value to the datapoint
     * @param value is the new value for the datapoint
     * @throws IOException
     */
    public void setValue(float value) throws IOException {}

    /**
     * this method returns the boolean value contained into the datapoint
     * @return the value contained into the datapoint
     */
    public boolean getValueB() {
        return false;
    }

    /**
     * this method returns the float value contained into the datapoint
     * @return the value contained into the datapoint
     */
    public float getValueF() {
        return 0;
    }

    /**
     * Method String getLabel()
     * @return string label of variable label
     */
    public String getLabel(){
        return DataPoint.this.label;
    }


    /**
     * Method boolean isOutput()
     * @return value of variable isOutput (true or false)
     */
    public boolean isOutput(){
        return isOutput;
    }

    public static void main(String[] args) throws InterruptedException, IOException {
        WebConnector wb = WebConnector.getInstance();
        wb.start();

        //Instantiate some datapoints
        BinaryDataPoint binp1 = new BinaryDataPoint("REMOTE_WIND_SW", true);
        BinaryDataPoint binp2 = new BinaryDataPoint("REMOTE_SOLAR_SW", true);
        FloatDataPoint fp1 = new FloatDataPoint("SOLAR_P_FLOAT", false);
        FloatDataPoint fp2 = new FloatDataPoint("COAL_P_FLOAT", false);

        //Change the value of those datapoints
        binp1.setValue(false);
        binp2.setValue(false);
        fp1.setValue(0.1f);
        fp2.setValue(0.5f);
        /*while(true){
            fdp1.setValue(700);
            fdp2.setValue(1700);
            Thread.sleep(1000);
            fdp1.setValue(2000);
            fdp2.setValue(1000);
            Thread.sleep(1000);
            fdp1.setValue(100);
            fdp2.setValue(1800);
            Thread.sleep(1000);
        }*/
    }

}
