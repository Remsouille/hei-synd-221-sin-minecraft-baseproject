# Minecraft EA Project

IntelliJ base project related to Minecraft Lab of 221_SIn course.
We have developed a program for managing a power grid in Java.
To do this we used a Web page and a database to send and save data via a ModBus protocol. The power grid is represented as a Minecraft world.

## Getting Started

In order to get the complete project, you have to fork this project via git in your git's projects and then clone it in your programming environment (see below in Installing)

For a better understanding of the project in his globality, refer to [Javadoc index](https://gitlab.com/Remsouille/hei-synd-221-sin-minecraft-baseproject/-/tree/master/javadoc/hei) (you need to download the directory to open the page)

### Prerequisites

To use and test the project :
* IntelliJ IDEA 2020.3 , 
[IntelliJ IDEA](https://www.jetbrains.com/fr-fr/idea/download/#section=windows) - Programming environment for Java

* [Maven](https://maven.apache.org/) - Dependency Management (included in IntelliJ IDEA)

* Grafana , 
[Grafana](https://grafana.sdi.hevs.ch/d/cpHGG93Gk/demo?refresh=5s&orgId=32) - Graphical data visualisation software

* Index.html , 
[Web page](https://cyberlearn.hes-so.ch/course/view.php?id=16350) - Web site for Minecraft SIn world

* Minecraft SIn world ,
[SIn world](https://cyberlearn.hes-so.ch/course/view.php?id=16350) - Power grid server

### Installing

1) Launch IntelliJ IDEA [git Project](https://gitlab.com/Remsouille/hei-synd-221-sin-minecraft-baseproject) 
```
click on "get from version control" then enter the url of the git project to clone it by clicking on "clone" button.
```

2) Start SIn world on Minecraft
```
open cmd window and enter the path to the Minecraft SIn world folder then enter the line "gradlew runClient". The game will start right after.
```

3) run the main (Launcher) of the project on IntelliJ
```
Select the right class to launch the entire project - here it is "Launcher" class.
```

4) Open Web page 
```
Once you opened the page you should refresh it to see  the message "Welcome to server !".
```
5) Open Grafana and log in
```
Use the link given "in Getting Started" above and log in with the correct login and password
(SIn31 : 47298ef110acefc80b0aa8942dd6b730).
```
**After these 5 steps your project should be all set up to start working and testing**

## Running the tests

If you want to run the different components individually, you should change the "class to run" in IntelliJ. You can do it by clicking on the "class to run" option on the left of the run button.

If you want to run the whole project, you need to run the class Launcher.

### Break down into end to end tests

1) Core component (digital twin)

Testing this component will display on the console whenever there is a change between the field and the database/Web page
For exemple :
```
pushToDB: REMOTE_WIND_SW value=0
Body: SInWorld REMOTE_WIND_SW=0
204
pushToWeb: REMOTE_WIND_SW=0
pushToField: REMOTE_WIND_SW=0
```

2) ModBus component (communication protocol)

Testing this component will display on the console read/write operations between the programming environment and the power grid on the SIn world. It will also modify directly states and value on the SIn world.
For exemple :
```
000100000006010102650001 <-- full frame array content from modbusBuffer1 readCoil
the coil value is : 1
0001000000060103025d0002 <-- full frame array content from modbusBuffer1 readReg
the value is : 20.0%
00010000000601050191ff00 <-- full frame array content from modbusBuffer1 writeCoil
00020000000b011000cd0002043e4ccccd <-- full frame array content from modbusBuffer1 writeReg
```

3) Field component

Testing this component will display on the console a periodic update of every registers and write operation on registers when called
For exemple :
```
It will show the same result as 1) & 2) but periodically
```

4) Database component
Testing this component will display on the console the body of the HTML message and the message from the server.
For exemple :
```
pushToDB: REMOTE_WIND_SW value=0
Body: SInWorld REMOTE_WIND_SW=0
204
```
204 mean that the server didn't have a problem

To test correctly this component we need to use the main branch and use the main in launcher

5) Web component
Testing this component will display on the console a message when a page is connected and a message when the web page modify some value.
For exemple :
```
127.0.0.1 joined the server!
New message from : 127.0.0.1 who say : REMOTE_COAL_SP=0.81
```

6) Smartcontrol component

Sadly the SmartControl component has not been implemented since we encountered difficulties on the field component
Testing this component will display oln the console the same result as 3) and will automatically update all SIn world values for a 50% charge on the battery

## Authors

* **Rémy Oreiller** & **Benjamin Caillet** - *Initial work* - [SIn-Minecraft-BaseProject](https://gitlab.com/patrudaz/hei-synd-221-sin-minecraft-baseproject)

## Acknowledgments

* Mr **Gabioud** and Mr **Rudaz** for their help during and outside the project periods
* Inspiration from cyberlearn's documentations
* Understanding of the mbap header content [simplyModbus](https://www.simplymodbus.ca/TCP.htm)
